package bankaccountapp;

/**
 * Created by blair on 07/05/18.
 */
public interface IBaseRate {

    default double getBaseRate() {
        return 2.5;
    }
}
