package bankaccountapp;

/**
 * Created by blair on 07/05/18.
 */
public class SavingsAccount extends Account {

    int safetyDepositBoxID;
    int safetyDepositBoxKey;

    public SavingsAccount(String name, String sSN, double initDeposit) {
        super(name, sSN, initDeposit);
        accountNumber = "1" + accountNumber;
        setSafetyDepositBox();
    }

    @Override
    public void setRate() {
        rate = getBaseRate() - .25;
    }

    private void setSafetyDepositBox() {
        safetyDepositBoxID = (int) (Math.random() * Math.pow(10, 3));
        safetyDepositBoxKey = (int) (Math.random() * Math.pow(10, 4));
    }

    public void showInfo() {
        super.showInfo();
        System.out.println("Savings Account");
        System.out.println("Safety Deposit box ID: " +safetyDepositBoxID + "\n Safety deposit box key: " + safetyDepositBoxKey );
    }
}
