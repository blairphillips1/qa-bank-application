package bankaccountapp;

/**
 * Created by blair on 07/05/18.
 */
public class CheckingAccount extends Account {

    int debitCardNumber;
    int debitCardPin;

    public CheckingAccount(String name, String sSN, double initDeposit) {
        super(name, sSN, initDeposit);

        accountNumber = "2" + accountNumber;

        setDebitCard();
    }

    @Override
    public void setRate() {
       rate = getBaseRate() * 0.15;
    }

    private void setDebitCard() {
        debitCardNumber = (int) (Math.random() * Math.pow(10, 12));
        debitCardPin = (int) (Math.random() * Math.pow(10, 4));
    }

    public void showInfo() {
        super.showInfo();
        System.out.println("Checking Account");
        System.out.println("Debit Card Number : " + debitCardNumber + "\n Debit Card Pin: " + debitCardPin );
    }
}
