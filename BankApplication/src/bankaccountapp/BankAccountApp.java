package bankaccountapp;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by blair on 07/05/18.
 */
public class BankAccountApp {

    public static void main(String[] args){
        List<Account> accounts = new LinkedList<Account>();

        String file = "C:\\Users\\Administrator\\Downloads\\NewBankAccounts.csv";

        List<String[]> newAccountHolders = utilites.ReadingFromCSV.read(file);
        for(String[] accountHolder: newAccountHolders) {
            System.out.println(accountHolder[0]);
            String name = accountHolder[0];
            String sSN = accountHolder[1];
            String accountType = accountHolder[2];
            double initDeposit = Double.parseDouble(accountHolder[3]);
            System.out.println(name + " " + sSN + " " + accountType + " £" + initDeposit);
            if(accountType.equals("Savings")) {
                System.out.println("Open a savings account");
            } else if (accountType.equals("Checking")) {
                System.out.println("Open a checking account");
            } else {
                System.out.println("Error");
            }

        }

        for (Account acc: accounts){
            System.out.println("**********");
            acc.showInfo();
        }
    }
}
